*** NOTE ***

To use these scripts, you must create top-level /Input and /Output directories.
I have intentionally gitignored those directories so that the repo doesn't get
cluttered with pre- and post-processed files.

Use the requirements.txt file to install needed packages.

Enjoy.

Author: Nick Burleigh for Wood Mackenzie, 2020