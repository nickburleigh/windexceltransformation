import pandas as pd
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
from tkinter import simpledialog
import sys

# remove the tkinter GUI
root = tk.Tk()
root.withdraw()

# ask for the file
input_filepath = filedialog.askopenfilename(filetypes=[('Excel file', '.xls .xlsx'), ('CSV files', '*.csv')],
                                       title='Please select the projects file to transform.')
# if they hit "cancel"
if input_filepath == '':
    sys.exit(1)

# prompt for the quarter, year of the data
# I intentionally did not automate this, so that you can do previous files with correct output labeling
datemonth = simpledialog.askstring('Date',
         'Please enter the year & quarter of the file in the format YYYY-QQ\nFor example, 2021-Q1')

# import the projects file -- but be sure to check for any changed column headers or added/subtracted columns
projects_no_regions = pd.read_excel(input_filepath,
              sheet_name='Global Installed Projects',
              skiprows=16,
              usecols="B:R")

# strip whitespace from 'Country'
projects_no_regions['Country'] = projects_no_regions['Country'].str.strip()

# import geography mapper
geo_map = pd.read_excel('geography/geography_mapping.xlsx')

# rename the Region column
geo_map.rename({'Primary Region':'Region'}, axis=1, inplace=True)

# remove the sub-regions column
geo_map_regions = geo_map[['Country', 'Region']].copy()

# add Region column to the projects
projects = pd.merge(projects_no_regions, geo_map_regions, on='Country', how='left')

# get the output directory
output_directory = filedialog.askdirectory(title='Please select a location to place the transformed file.')
# if they hit "cancel"
if output_directory == '':
    sys.exit(1)

# output
projects.to_excel(output_directory + '/Projects Transformed - ' + datemonth + '.xlsx', index=False)


# let 'em know it's done
messagebox.showinfo('Done!', 'Your file is ready.')
