import pandas as pd
import numpy as np
from tkinter import simpledialog
from tkinter import filedialog
from tkinter import messagebox
import tkinter as tk
import sys

# remove the tkinter GUI
root = tk.Tk()
root.withdraw()

# ask for the year/quarter in the format YYYY-QQ
# I did not automate this, so that you can do previous files without incorrect output labeling
date = simpledialog.askstring('Date',
         'Please enter the year & quarter of the file in the format YYYY-QQ\nFor example, 2021-Q1')
###

# ask for the file
input_filepath = filedialog.askopenfilename(filetypes=[('Excel file', '.xls .xlsx .xlsm'), ('CSV files', '*.csv')],
                                       title='Please select the "MOU - Master" file to transform.')
# if they hit "cancel"
if input_filepath == '':
    sys.exit(1)


# List of labeled rows to exclude (these are just rows with unnecessary headings [formatting], not rows with data)
excl = [np.nan,'','North America','Total North America','South America','Total South America',
                'Caribbean','Total Caribbean','Central America','Total Central America','Total Latin America',
                'Total Latin America (Excl. Brazil)','Total Americas','Northern Europe','Total Northern Europe',
                'Western Europe','Total Western Europe','Southern Europe','Total Southern Europe','Eastern Europe',
                'Total Eastern Europe','Russia and the Caspian','Total Russia and the Caspian',
                'Total Eastern Europe plus Russia and the Caspian','Total Europe without Russian and the Caspian',
                'Total Europe', 'Africa','Total Africa','Middle East','Total Middle East','Total Middle East & Africa',
                'Total EMEA (not including RC)','Total EMEA','Asia Pacific','Total Asia Pacific',
                'Total Asia Pacific (ex-China)','Total Asia Pacific (ex-China and India)','Global Total',
                'Global Total (Excl. China)','Total EMEARC']

# read in the geography_mapping file, for use later

geo_map = pd.read_excel('geography/geography_mapping.xlsx')

# read in the onshore - New Build tab & transform
on_nb = pd.read_excel(input_filepath,
              sheet_name='Onshore–New Build',
              skiprows=6,
              skipfooter=10,
              usecols="B:AI")
on_nb.rename({'Unnamed: 1':'Country'}, axis='columns', inplace=True)
on_nb = on_nb[~on_nb.Country.isin(excl)]

# read in the onshore - repowering tab & transform
on_re = pd.read_excel(input_filepath,
              sheet_name='Onshore–Repowering',
              skiprows=6,
              skipfooter=10,
              usecols="B:AI")
on_re.rename({'Unnamed: 1':'Country'}, axis='columns', inplace=True)
on_re = on_re[~on_re.Country.isin(excl)]

# read in the onshore - decommissioning tab & transform
on_de = pd.read_excel(input_filepath,
              sheet_name='Onshore–Decommissioning',
              skiprows=6,
              skipfooter=10,
              usecols="B:AI")
on_de.rename({'Unnamed: 1':'Country'}, axis='columns', inplace=True)
on_de = on_de[~on_de.Country.isin(excl)]

# read in the offshore - new build tab & transform
off_nb = pd.read_excel(input_filepath,
              sheet_name='Offshore-New Build',
              skiprows=6,
              skipfooter=10,
              usecols="B:AI")
off_nb.rename({'Unnamed: 1':'Country'}, axis='columns', inplace=True)
off_nb = off_nb[~off_nb.Country.isin(excl)]

# read in the offshore - repowering tab & transform
off_re = pd.read_excel(input_filepath,
              sheet_name='Offshore–Repowering',
              skiprows=6,
              skipfooter=10,
              usecols="B:AI")
off_re.rename({'Unnamed: 1':'Country'}, axis='columns', inplace=True)
off_re = off_re[~off_re.Country.isin(excl)]

# read in the offshore - decommissioning tab & transform
off_de = pd.read_excel(input_filepath,
              sheet_name='Offshore–Decommissioning',
              skiprows=6,
              skipfooter=10,
              usecols="B:AI")
off_de.rename({'Unnamed: 1':'Country'}, axis='columns', inplace=True)
off_de = off_de[~off_de.Country.isin(excl)]

# de-pivot all the dataframes
on_nb = on_nb.melt(id_vars='Country', var_name='Year', value_name='Capacity')
on_de = on_de.melt(id_vars='Country', var_name='Year', value_name='Capacity')
on_re = on_re.melt(id_vars='Country', var_name='Year', value_name='Capacity')
off_nb = off_nb.melt(id_vars='Country', var_name='Year', value_name='Capacity')
off_de = off_de.melt(id_vars='Country', var_name='Year', value_name='Capacity')
off_re = off_re.melt(id_vars='Country', var_name='Year', value_name='Capacity')

# add site & status columns
on_nb['Site'] = 'Onshore'
on_de['Site'] = 'Onshore'
on_re['Site'] = 'Onshore'
off_nb['Site'] = 'Offshore'
off_de['Site'] = 'Offshore'
off_re['Site'] = 'Offshore'

on_nb['Status'] = 'New Build'
on_de['Status'] = 'Decommissioning'
on_re['Status'] = 'Repowering'
off_nb['Status'] = 'New Build'
off_de['Status'] = 'Decommissioning'
off_re['Status'] = 'Repowering'

# concatenate all the dataframes into one
all_no_regions = pd.concat([on_nb,on_de,on_re,off_nb,off_de,off_re], axis=0)

# merge the Primary Region & Secondary Region columns in the geo_map dataframe, on Country
all = pd.merge(all_no_regions, geo_map, on='Country', how='left')

# get the output directory
output_directory = filedialog.askdirectory(title='Please select a location to place the transformed file.')
# if they hit "cancel"
if output_directory == '':
    sys.exit(1)

all.to_excel(output_directory + '/MOU Transformed - ' + date + '.xlsx', index=False)

# let 'em know it's done
messagebox.showinfo('Done!', 'Your file is ready.')
