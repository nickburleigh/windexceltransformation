import pandas as pd
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
from tkinter import simpledialog
import sys
import pprint

# remove the tkinter GUI
root = tk.Tk()
root.withdraw()

# ask for the file
input_filepath = filedialog.askopenfilename(filetypes=[('Excel file', '.xls .xlsx'), ('CSV files', '*.csv')],
                                       title='Please select the orders file to transform.')

# if they hit "cancel"
if input_filepath == '':
    sys.exit(1)

# ask for the previous month's date in the format YYYY-MM -- DEPRECATED
# I did not automate this, so that you can do previous files without incorrect output labeling
# datemonth = simpledialog.askstring('Date',
#          'Please enter the month & year of the file in the format YYYY-MM\nFor example, January 2021 should be 2021-01')

# do the transform -- NOTE: you have to manually changed the years forward each year
trans_no_subregions = pd.read_excel(input_filepath,
              sheet_name='Asset transactions',
              skiprows=9,
              usecols="B:AB")

trans_no_subregions.rename({'Project country':'Country'}, axis=1, inplace=True)

# strip whitespace from 'Country'
trans_no_subregions['Country'] = trans_no_subregions['Country'].str.strip()

# import geography mapper
geo_map = pd.read_excel('geography/geography_mapping.xlsx')

# rename the Subregion column
geo_map.rename({'Secondary Region':'Subregion'}, axis=1, inplace=True)

# remove the sub-regions column
geo_map_regions = geo_map[['Country', 'Subregion']].copy()

# add Region column to the projects
trans = pd.merge(trans_no_subregions, geo_map_regions, on='Country', how='left')

# get the output directory
output_directory = 'C:/Users/burlni/OneDrive - Verisk Analytics/Wind files for data hub'

# output
writer = pd.ExcelWriter(output_directory + '/Asset Transactions Transformed.xlsx', engine='xlsxwriter',
                        options={'strings_to_urls': False})
trans.to_excel(writer, index=False)
writer.close()


# let 'em know it's done
messagebox.showinfo('Done!', 'Your file is ready.')
