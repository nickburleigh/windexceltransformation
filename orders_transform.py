import pandas as pd
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
from tkinter import simpledialog
import sys

# remove the tkinter GUI
root = tk.Tk()
root.withdraw()

# ask for the file
input_filepath = filedialog.askopenfilename(filetypes=[('Excel file', '.xls .xlsx'), ('CSV files', '*.csv')],
                                       title='Please select the orders file to transform.')
# if they hit "cancel"
if input_filepath == '':
    sys.exit(1)

# ask for the previous month's date in the format YYYY-MM
# I did not automate this, so that you can do previous files without incorrect output labeling
datemonth = simpledialog.askstring('Date',
         'Please enter the month & year of the file in the format YYYY-MM\nFor example, January 2021 should be 2021-01')

# do the transform -- NOTE: you have to manually changed the years forward each year
orders = pd.read_excel(input_filepath,
              sheet_name='Order Tracking ',
              skiprows=16,
              skipfooter=2,
              usecols="B:AC")


MW = orders.melt(id_vars=['Supplier', 'Region', 'Sub-region', 'Country', 'Project location',
       'Project name', 'Project name (Chinese)', 'Wind Base (China)', 'Buyer', 'Order Date', 'Order Status', 'Project Type',
       'Order MW', 'Turbine Model', 'Turbine #', 'MW Rating'],
        value_vars=['2021', '2022', '2023', '2024', '2025', '2026'],
        value_name='Capacity', var_name='Year')


turbs = orders.melt(id_vars=['Supplier', 'Region', 'Sub-region', 'Country', 'Project location',
       'Project name', 'Project name (Chinese)', 'Wind Base (China)', 'Buyer', 'Order Date', 'Order Status', 'Project Type',
       'Order MW', 'Turbine Model', 'Turbine #', 'MW Rating'],
        value_vars=['´2021', '\'2022', '\'2023', '\'2024', '\'2025', '\'2026'], # make sure the apostrophes are the right kind!
        value_name='Turbines', var_name='Year')

turbs.replace({'´2021':'2021','\'2022':'2022', '\'2023':'2023', '\'2024':'2024', '\'2025':'2025', '\'2026':'2026'}, inplace=True)

turbs = turbs[['Turbines']].copy()

trans = pd.concat([MW, turbs], axis=1)

# get the output directory
output_directory = filedialog.askdirectory(title='Please select a location to place the transformed file.')
# if they hit "cancel"
if output_directory == '':
    sys.exit(1)

# output
trans.to_excel(output_directory + '/Orders Transformed - ' + datemonth + '.xlsx', index=False)


# let 'em know it's done
messagebox.showinfo('Done!', 'Your file is ready.')
