import pandas as pd
from tkinter import simpledialog
from tkinter import filedialog
from tkinter import messagebox
import tkinter as tk
import sys

# remove the tkinter GUI
root = tk.Tk()
root.withdraw()

# ask for the year in the format YYYY
# I did not automate this, so that you can do previous files without incorrect output labeling
year = simpledialog.askstring('Which year?',
         'Please enter the year of the file using all four digits.')

# ask for the file
input_filepath = filedialog.askopenfilename(filetypes=[('Excel file', '.xls .xlsx .xlsm'), ('CSV files', '*.csv')],
                                       title='Please select the orders file to transform.')
# if they hit "cancel"
if input_filepath == '':
    sys.exit(1)

# read in the data, usecols needs to be updated annually to include the new year
ms = pd.read_excel(input_filepath,
                   sheet_name='Market Share',
                   skiprows=14,
                   skipfooter=1,
                   usecols="B:S")

# NOTE: Add the new year below
melted = ms.melt(id_vars=['Country', 'Region', 'Subregion', 'Turbine OEM', 'Segment'],
                 value_vars=['<2009', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018',
                             '2019', '2020'],
                 value_name='Capacity',
                 var_name='Year')

melted['Activity'] = 'Connected'

melted.rename({'Subregion': 'Sub-region', 'Turbine OEM': 'Vendor'}, axis=1, inplace=True)

# get the output directory
output_directory = filedialog.askdirectory(title='Please select a location to place the transformed file.')
# if they hit "cancel"
if output_directory == '':
    sys.exit(1)

melted.to_excel(output_directory + '/OEM Market Share Transformed - ' + year + '.xlsx', index=False)

# let 'em know it's done
messagebox.showinfo('Done!', 'Your file is ready.')
